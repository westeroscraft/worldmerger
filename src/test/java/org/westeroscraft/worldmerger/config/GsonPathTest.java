package org.westeroscraft.worldmerger.config;

import static org.junit.Assert.assertEquals;

import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

/**
 * Tests for GsonPath, the JSON (de)serializer for Path.
 * @author joskuijpers
 *
 */
public class GsonPathTest {
	private Gson gson;
	
	/**
	 * Run before every test.
	 */
	@Before
	public void before() {
		gson = new GsonBuilder().registerTypeAdapter(Path.class, new Configuration.GsonPath())
				.create();
	}

	/**
	 * Test serialization.
	 */
	@Test
	public void testSerialization() {
		JsonTestModel model;
		String json;
		
		json = "{\"path\":\"/hello/world/\"}";
		model = gson.fromJson(json, JsonTestModel.class);
		
		assertEquals(FileSystems.getDefault().getPath("/", "hello", "world"), model.getPath());
	}

	/**
	 * Test deserialization.
	 */
	@Test
	public void testDeserialization() {
		String json;
		JsonTestModel model;
		Path path;
		
		model = new JsonTestModel();
		path = FileSystems.getDefault().getPath("hello", "world");
		model.setPath(path);

		json = gson.toJson(model);

		assertEquals("{\"path\":\"" + path + "\"}", json);
	}
	
	/**
	 * Test an invalid path.
	 */
	@Test(expected = JsonParseException.class)
	public void testInvalidPath() {
		String json;
		
		json = "{\"path\":\"\\C:\\S\0ample\"}";
		gson.fromJson(json, JsonTestModel.class).getPath();
	}
	
	/**
	 * A test model.
	 */
	class JsonTestModel {
		private Path path;

		/**
		 * @return the path
		 */
		public Path getPath() {
			return path;
		}

		/**
		 * @param path the path to set
		 */
		public void setPath(Path path) {
			this.path = path;
		}
	}

}
