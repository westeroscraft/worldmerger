package org.westeroscraft.worldmerger.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import org.junit.Test;

/**
 * Tests for XZPair.
 * 
 * @author joskuijpers
 */
public class XZPairTest {

	/**
	 * Test getters and arg constructor.
	 */
	@Test
	public void testGetters() {
		XZPair pair;
		
		pair = new XZPair(1, 2);

		assertSame(1, pair.getX());
		assertSame(2, pair.getZ());
	}
	
	/**
	 * Test setters and no-arg constructor.
	 */
	@Test
	public void testSetters() {
		XZPair pair = new XZPair();
		
		pair.setX(5);
		pair.setZ(10);

		assertSame(5, pair.getX());
		assertSame(10, pair.getZ());
	}

	/**
	 * Test the equals method.
	 */
	@Test
	public void testEquals() {
		XZPair pair1, pair2, pair3;

		pair1 = new XZPair(1, 2);
		pair2 = new XZPair(1, 2);
		pair3 = new XZPair(2, 1);

		assertEquals(pair1, pair2);
		assertNotEquals(pair2, pair3);
		assertNotEquals("Hello World", pair1);
		assertNotEquals(pair1, "Hello World");
	}

	/**
	 * Test the hashCode method.
	 */
	@Test
	public void testHashCode() {
		XZPair pair1, pair2, pair3;

		pair1 = new XZPair(1, 2);
		pair2 = new XZPair(1, 2);
		pair3 = new XZPair(2, 1);

		assertSame(pair1.hashCode(), pair2.hashCode());
		assertNotSame(pair2.hashCode(), pair3.hashCode());
		assertNotSame("Hello World", pair1.hashCode());
	}
	
	/**
	 * Test the toString method.
	 */
	@Test
	public void testToString() {
		XZPair pair = new XZPair(1, 2);
		
		assertEquals("<1,2>", pair.toString());
	}
}
