package org.westeroscraft.worldmerger.config;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

/**
 * Tests for GsonXZPair, the JSON (de)serializer for XZPair.
 * 
 * @author joskuijpers
 */
public class GsonXZPairTest {
	private Gson gson;
	
	/**
	 * Run before every test.
	 */
	@Before
	public void before() {
		gson = new GsonBuilder().registerTypeAdapter(XZPair.class, new XZPair.GsonXZPair())
				.create();
	}
	
	/**
	 * Test serialization.
	 */
	@Test
	public void testJsonSerialization() {
		JsonTestModel model;
		String json;
		
		json = "{\"pair\":[5,2]}";
		model = gson.fromJson(json, JsonTestModel.class);
		
		assertEquals(new XZPair(5, 2), model.getPair());
	}
	
	/**
	 * Test serialization of bad pair.
	 */
	@Test(expected = JsonParseException.class)
	public void testJsonSerializationBadPair() {
		String json;
		
		json = "{\"pair\":[5,2,3]}";
		gson.fromJson(json, JsonTestModel.class);
	}
	
	/**
	 * Test deserialization.
	 */
	@Test
	public void testJsonDeserialization() {
		String json;
		JsonTestModel model;
		
		model = new JsonTestModel();
		model.setPair(new XZPair(3, 7));

		json = gson.toJson(model);

		assertEquals("{\"pair\":[3,7]}", json);
	}

	/**
	 * A test model.
	 */
	class JsonTestModel {
		private XZPair pair;

		/**
		 * @return the pair
		 */
		public XZPair getPair() {
			return pair;
		}

		/**
		 * @param pair the pair to set
		 */
		public void setPair(XZPair pair) {
			this.pair = pair;
		}
	}
}
