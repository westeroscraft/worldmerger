package org.westeroscraft.worldmerger.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;

import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.junit.Test;

/**
 * Tests for Part class.
 * 
 * @author joskuijpers
 */
public class PartTest {

	/**
	 * Test the getters and setters.
	 * 
	 * As this is a POJO class, it is nothing more than setters and getters.
	 */
	@Test
	public void testGettersAndSetters() {
		Part part = new Part();

		part.setOrigin(new XZPair(1, 2));
		assertEquals(new XZPair(1, 2), part.getOrigin());

		part.setSize(new XZPair(3, 4));
		assertEquals(new XZPair(3, 4), part.getSize());

		part.setOut(new XZPair(5, 6));
		assertEquals(new XZPair(5, 6), part.getOut());
	}
	
	/**
	 * Test the toString method.
	 */
	@Test
	public void testToString() {
		Part part;
		Path path;

		part = new Part();
		path = FileSystems.getDefault().getPath("hello", "world");

		part.setPath(path);
		part.setOrigin(new XZPair(1, 2));
		part.setSize(new XZPair(3, 4));
		part.setOut(new XZPair(5, 6));
		
		assertEquals("{path: " + path.toString() + ", origin: <1,2>, size: <3,4>, out: <5,6>}",
				part.toString());
	}
	
	/**
	 * Test equals().
	 */
	@Test
	public void testEquals() {
		Part part1, part2;
		Path path;

		part1 = new Part();
		part2 = new Part();
		
		path = FileSystems.getDefault().getPath("hello", "world");

		part1.setPath(path);
		part1.setOrigin(new XZPair(1, 2));
		part1.setSize(new XZPair(3, 4));
		part1.setOut(new XZPair(5, 6));
		
		assertEquals(part1, part1);
		assertNotEquals(part1, part2);
		assertNotEquals(part1, "");
	}
	
	/**
	 * Test hashCode().
	 */
	@Test
	public void testHashCode() {
		Part part1, part2;
		Path path;
		
		part1 = new Part();
		part2 = new Part();
		
		path = FileSystems.getDefault().getPath("hello", "world");

		part1.setPath(path);
		part1.setOrigin(new XZPair(1, 2));
		part1.setSize(new XZPair(3, 4));
		part1.setOut(new XZPair(5, 6));
		
		assertEquals(part1.hashCode(), part1.hashCode());
		assertNotSame(part1.hashCode(), part2.hashCode());
		
		part2.setPath(path);
		part2.setOrigin(new XZPair(1, 2));
		part2.setSize(new XZPair(3, 4));
		part2.setOut(new XZPair(5, 6));
		assertEquals(part1.hashCode(), part2.hashCode());
	}
}
