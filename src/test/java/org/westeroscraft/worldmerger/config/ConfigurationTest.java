package org.westeroscraft.worldmerger.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Test;

/**
 * Tests for Configuration class.
 * 
 * @author joskuijpers
 *
 */
public class ConfigurationTest {

	/**
	 * Test loading config from file.
	 * 
	 * @throws URISyntaxException
	 *             when the file system is stupid
	 * @throws IOException
	 *             when reading of the test file fails
	 * @throws InvalidConfigurationException
	 *             when the configuration file is invalid
	 */
	@Test
	public void testLoading()
			throws URISyntaxException, IOException, InvalidConfigurationException {
		Configuration config;
		List<Part> parts;
		Part part;

		config = load("configtest1.json");

		assertEquals(1.0, config.getVersion(), 0.001);

		assertNotNull(config.getOutput());
		assertEquals(FileSystems.getDefault().getPath("examples", "worlds", "testOut"), 
				config.getOutput().getPath());

		parts = config.getParts();
		assertNotNull(parts);
		assertSame(parts.size(), 1);

		part = parts.get(0);
		assertEquals(new XZPair(0, 2048), part.getOrigin());
		assertEquals(new XZPair(2048, 1024), part.getSize());
		assertEquals(new XZPair(0, 512), part.getOut());
		assertEquals(FileSystems.getDefault().getPath("examples", "worlds", "testIn"),
				part.getPath());
	}
	
	/**
	 * Load a test config file.
	 * 
	 * @param name name of the resource file in test/resources
	 * @return Configuration
	 * @throws URISyntaxException
	 *             when the file system is stupid
	 * @throws IOException
	 *             when reading of the test file fails
	 * @throws InvalidConfigurationException
	 *             when the configuration file is invalid
	 */
	Configuration load(String name)
			throws URISyntaxException, IOException, InvalidConfigurationException {
		URL resource;
		Path path;
		Configuration config;
		
		resource = Thread.currentThread().getContextClassLoader().getResource(name);
		path = Paths.get(resource.toURI());

		config = Configuration.load(path);
		
		return config;
	}
	
	/**
	 * Test loading config from file with non-region aligned coordinates.
	 * 
	 * @throws URISyntaxException
	 *             when the file system is stupid
	 * @throws IOException
	 *             when reading of the test file fails
	 * @throws InvalidConfigurationException
	 *             when the configuration file is invalid
	 */
	@Test(expected = InvalidConfigurationException.class)
	public void testLoadingWithUnalignedCoordinates()
			throws URISyntaxException, IOException, InvalidConfigurationException {
		load("configtest2fail.json");
	}
	
	/**
	 * Test loading config from file with no parts.
	 * 
	 * @throws URISyntaxException
	 *             when the file system is stupid
	 * @throws IOException
	 *             when reading of the test file fails
	 * @throws InvalidConfigurationException
	 *             when the configuration file is invalid
	 */
	@Test(expected = InvalidConfigurationException.class)
	public void testLoadingWithNoParts()
			throws URISyntaxException, IOException, InvalidConfigurationException {
		load("configtest3fail.json");
	}
	
	/**
	 * Test loading config from file with no output description.
	 * 
	 * @throws URISyntaxException
	 *             when the file system is stupid
	 * @throws IOException
	 *             when reading of the test file fails
	 * @throws InvalidConfigurationException
	 *             when the configuration file is invalid
	 */
	@Test(expected = InvalidConfigurationException.class)
	public void testLoadingWithNoOutput()
			throws URISyntaxException, IOException, InvalidConfigurationException {
		load("configtest4fail.json");
	}
	
	/**
	 * Test saving config to file.
	 * 
	 * @throws URISyntaxException
	 *             when the file system is stupid
	 * @throws IOException
	 *             when reading or writing of the test file fails
	 * @throws InvalidConfigurationException
	 *             when the configuration file is invalid
	 */
	@Test
	public void testSaving() throws URISyntaxException, IOException, InvalidConfigurationException {
		Configuration config, configSaved;
		Path path;
		
		config = load("configtest1.json");
		
		path = FileSystems.getDefault().getPath("tmp.json");
		config.save(path);
		
		assertTrue(Files.exists(path));
		
		configSaved = Configuration.load(path);
		Files.delete(path);
		
		assertEquals(config, configSaved);
	}

	/**
	 * Test toString.
	 * @throws URISyntaxException
	 *             when the file system is stupid
	 * @throws IOException
	 *             when reading or writing of the test file fails
	 * @throws InvalidConfigurationException
	 *             when the configuration file is invalid
	 */
	@Test
	public void testToString()
			throws URISyntaxException, IOException, InvalidConfigurationException {
		Configuration config;
		
		config = load("configtest1.json");
		
		assertEquals("{version: 1.0, output: {path: examples/worlds/testOut}, parts: ["
				+ config.getParts().get(0) + "]}", config.toString());
	}
	
	/**
	 * Test equals.
	 * @throws URISyntaxException
	 *             when the file system is stupid
	 * @throws IOException
	 *             when reading or writing of the test file fails
	 * @throws InvalidConfigurationException
	 *             when the configuration file is invalid
	 */
	@Test
	public void testEquals() throws URISyntaxException, IOException, InvalidConfigurationException {
		Configuration config1, config2, config3;
		
		config1 = load("configtest1.json");
		config2 = load("configtest1.json");
		config3 = new Configuration();
		
		assertEquals(config1, config2);
		
		config1.setVersion(2.0);
		assertNotEquals(config1, config2);
		
		config1.setVersion(1.0);
		config1.getOutput().setPath(null);
		assertNotEquals(config1, config2);
		
		config2.getOutput().setPath(null);
		assertEquals(config1, config2);
		
		assertNotEquals(config1, config3);
		assertNotEquals(config1, "");
	}
	
	/**
	 * Test hashCode.
	 * @throws URISyntaxException
	 *             when the file system is stupid
	 * @throws IOException
	 *             when reading or writing of the test file fails
	 * @throws InvalidConfigurationException
	 *             when the configuration file is invalid
	 */
	@Test
	public void testHashCode()
			throws URISyntaxException, IOException, InvalidConfigurationException {
		Configuration config1, config2, config3;
		
		config1 = load("configtest1.json");
		config2 = load("configtest1.json");
		config3 = new Configuration();

		assertEquals(config1.hashCode(), config2.hashCode());
		assertNotEquals(config1.hashCode(), config3.hashCode());
	}
}
