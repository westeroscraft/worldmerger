package org.westeroscraft.worldmerger;

/**
 * Common functions for GUI and CLI.
 * 
 * @author joskuijpers
 */
public abstract class AppInterface {
	private String[] programArgs;
	
	/**
	 * Base constructor.
	 * @param args program arguments
	 */
	public AppInterface(String[] args) {
		this.setProgramArgs(args);
	}
	
	/**
	 * Start the interface.
	 */
	public abstract void run();

	/**
	 * @return the programArgs
	 */
	protected String[] getProgramArgs() {
		return programArgs;
	}

	/**
	 * @param programArgs the programArgs to set
	 */
	protected void setProgramArgs(String[] programArgs) {
		this.programArgs = programArgs;
	}
}
