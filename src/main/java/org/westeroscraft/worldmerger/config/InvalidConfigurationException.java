package org.westeroscraft.worldmerger.config;

/**
 * An exception for invalid configurations.
 * 
 * @author joskuijpers
 */
public class InvalidConfigurationException extends Exception {
	private static final long serialVersionUID = -9148002980787117005L;

	/**
	 * Create a new InvalidConfigurationException with given message.
	 * @param msg the message
	 */
	public InvalidConfigurationException(String msg) {
		super(msg);
	}
	
}
