package org.westeroscraft.worldmerger.config;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Configuration object, for loading and storing configurations.
 * 
 * @author joskuijpers
 */
public class Configuration {
	private double version;
	private Output output;
	private List<Part> parts;
	
	/**
	 * Simple no-args constructor.
	 */
	public Configuration() {
		parts = new ArrayList<Part>();
		output = new Output();
		version = 1.0;
	}
	
	/**
	 * @return the version
	 */
	public double getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(double version) {
		this.version = version;
	}

	/**
	 * @return the output
	 */
	public Output getOutput() {
		return output;
	}

	/**
	 * @return the parts
	 */
	public List<Part> getParts() {
		return parts;
	}

	/**
	 * Configure Gson for using custom types.
	 * 
	 * @return Gson object
	 */
	static Gson configureGson() {
		GsonBuilder builder = new GsonBuilder()
			.setPrettyPrinting()
			.serializeNulls()
			.setVersion(1.0);
		
		builder.registerTypeAdapter(Path.class, new GsonPath());
		builder.registerTypeAdapter(XZPair.class, new XZPair.GsonXZPair());
		
		return builder.create();
	}
	
	/**
	 * Load a configuration.
	 * 
	 * @param path path of the config file.
	 * @return configuration loaded configurartion
	 * @throws IOException when loading went bad.
	 * @throws InvalidConfigurationException when the configuration file is invalid
	 */
	public static Configuration load(Path path) throws IOException, InvalidConfigurationException {
		Gson gson = configureGson();
		String json = null;
		Configuration config;
		
		json = new String(Files.readAllBytes(path));
		config = gson.fromJson(json, Configuration.class);
		
		if (config.version == 0.0) {
			config.version = 1.0;
		}
		
		if (config.parts.size() < 1) {
			throw new InvalidConfigurationException("At least 1 part is required");
		}
		
		if (config.output == null || config.output.getPath() == null) {
			throw new InvalidConfigurationException("Missing output information");
		}
		
		for (Part part : config.parts) {
			if (!isPairRegionAligned(part.getOrigin()) || !isPairRegionAligned(part.getSize())
					|| !isPairRegionAligned(part.getOut())) {
				throw new InvalidConfigurationException(
						"XZ pairs must be region aligned block coordinates");
			}
		}
		
		return config;
	}
	
	private static boolean isPairRegionAligned(XZPair pair) {
		if (pair.getX() % 512 == 0 && pair.getZ() % 512 == 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Save this configuration to file.
	 * 
	 * @param path path to save to.
	 * @throws IOException when saving goes bad.
	 */
	public void save(Path path) throws IOException {
		Gson gson = configureGson();
		String json = null;
		
		json = gson.toJson(this);

		Files.write(path, json.getBytes());
	}
	
	@Override
	public String toString() {
		return "{version: " + getVersion() + ", output: " + getOutput() + ", parts: " + getParts()
				+ "}";
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Configuration) {
			Configuration other = (Configuration) o;

			if (other.version == version && other.output.equals(output)
					&& other.parts.equals(parts)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		int hash = ((Double) version).hashCode();

		if (parts != null) {
			hash ^= parts.hashCode();
		}

		if (output != null) {
			hash ^= output.hashCode();
		}

		return hash;
	}
	
	/**
	 * Output definition.
	 * 
	 * This is an object, for future compatibility.
	 * 
	 * @author joskuijpers
	 */
	public static class Output {
		private Path path;

		/**
		 * @return the path
		 */
		public Path getPath() {
			return path;
		}

		/**
		 * @param path the path to set
		 */
		public void setPath(Path path) {
			this.path = path;
		}
		
		@Override
		public String toString() {
			return "{path: " + path + "}";
		}
		
		@Override
		public boolean equals(Object o) {
			if (o instanceof Output) {
				Output other = (Output) o;
				
				if (other.path == null && path == null) {
					return true;
				}
				
				if (other.path != null && other.path.equals(path)) {
					return true;
				}
			}
			return false;
		}
		
		@Override
		public int hashCode() {
			int hash = 31;
			
			if (path != null) {
				hash ^= path.hashCode();
			}
			
			return hash;
		}
	}
	
	/**
	 * Serialization and deserialization for the Path class.
	 * 
	 * @author joskuijpers
	 */
	static class GsonPath implements JsonSerializer<Path>,
			JsonDeserializer<Path> {
		
		/**
		 * Basic constructor for registration.
		 */
		public GsonPath() {
		}

		@Override
		public Path deserialize(JsonElement elem, Type type,
				JsonDeserializationContext context) throws JsonParseException {
			Path p;
			
			try {
				p = FileSystems.getDefault().getPath(elem.getAsString());
			} catch (InvalidPathException e) {
				throw new JsonParseException("Path is invalid", e);
			}

			return p;
		}

		@Override
		public JsonElement serialize(Path path, Type type,
				JsonSerializationContext context) {
			return new JsonPrimitive(path.toString());
		}		
	}
}
