package org.westeroscraft.worldmerger.config;

import java.nio.file.Path;

/**
 * Definition of a Part, a world.
 * 
 * @author joskuijpers
 */
public class Part {
	private Path path;
	private XZPair origin;
	private XZPair size;
	private XZPair out;
	
	/**
	 * Empty no-args constructor.
	 */
	public Part() {
	}
	
	@Override
	public String toString() {
		return "{path: " + getPath() + ", origin: " + getOrigin() + ", size: " + getSize()
				+ ", out: " + getOut() + "}";
	}

	/**
	 * @return the path
	 */
	public Path getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(Path path) {
		this.path = path;
	}

	/**
	 * @return the origin
	 */
	public XZPair getOrigin() {
		return origin;
	}

	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(XZPair origin) {
		this.origin = origin;
	}

	/**
	 * @return the size
	 */
	public XZPair getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(XZPair size) {
		this.size = size;
	}

	/**
	 * @return the out
	 */
	public XZPair getOut() {
		return out;
	}

	/**
	 * @param out the out to set
	 */
	public void setOut(XZPair out) {
		this.out = out;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Part) {
			Part other = (Part) o;
			
			if (path.equals(other.path) && origin.equals(other.origin) && size.equals(other.size)
					&& out.equals(other.out)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		if (path != null) {
			hash ^= path.hashCode();
		}

		if (origin != null) {
			hash ^= origin.hashCode();
		}

		if (size != null) {
			hash ^= size.hashCode();
		}

		if (out != null) {
			hash ^= out.hashCode();
		}

		return hash;
	}
}