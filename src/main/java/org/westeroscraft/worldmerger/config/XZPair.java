package org.westeroscraft.worldmerger.config;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * An XZ pair, for points or sizes.
 * 
 * @author joskuijpers
 */
public class XZPair {
	private int x, z;
	
	/**
	 * Create an XZ pair.
	 * 
	 * @param x the x
	 * @param z the z
	 */
	public XZPair(int x, int z) {
		this.x = x;
		this.z = z;
	}
	
	/**
	 * No-args constructor.
	 */
	public XZPair() {
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the z
	 */
	public int getZ() {
		return z;
	}

	/**
	 * @param z the z to set
	 */
	public void setZ(int z) {
		this.z = z;
	}
	
	/**
	 * Convert the values from block to region.
	 * 
	 * Both the X and Z coordinate are divided by 512.
	 * 
	 * @return converted XZPair
	 */
	public XZPair toRegionPair() {
		return new XZPair(x >> 9, z >> 9);
	}

	/**
	 * Convert the values from region to block.
	 * 
	 * Both the X and Z coordinate are multiplied by 512.
	 * 
	 * @return converted XZPair
	 */
	public XZPair toBlockPair() {
		return new XZPair(x << 9, z << 9);
	}

	/**
	 * Convert the values from region to chunk.
	 * 
	 * Both the X and Z coordinate are multiplied by 32.
	 * 
	 * @return converted XZPair
	 */
	public XZPair toChunkPair() {
		return new XZPair(x << 5, z << 5);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof XZPair) {
			XZPair other = (XZPair) o;
			if (other.x == x && other.z == z) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return (x * 31) ^ z;
	}
	
	@Override
	public String toString() {
		return "<" + x + "," + z + ">";
	}
	
	/**
	 * Serialization and deserialization for the Path class.
	 * 
	 * @author joskuijpers
	 */
	static class GsonXZPair implements JsonSerializer<XZPair>, JsonDeserializer<XZPair> {
		/**
		 * Basic constructor for registration.
		 */
		public GsonXZPair() {
		}

		@Override
		public XZPair deserialize(JsonElement elem, Type type,
				JsonDeserializationContext context) throws JsonParseException {
			JsonArray array = elem.getAsJsonArray();
			XZPair pair = new XZPair();
			
			if (array.size() != 2) {
				throw new JsonParseException("XZ pair array must contain exactly 2 elements");
			}

			pair.setX(array.get(0).getAsInt());
			pair.setZ(array.get(1).getAsInt());
			
			return pair;
		}

		@Override
		public JsonElement serialize(XZPair pair, Type type,
				JsonSerializationContext context) {
			JsonArray array = new JsonArray();

			array.add(new JsonPrimitive(pair.getX()));
			array.add(new JsonPrimitive(pair.getZ()));
			
			return array;
		}		
	}
}
