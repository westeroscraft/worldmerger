package org.westeroscraft.worldmerger;

/**
 * Listener for events from the Merger.
 * 
 * Use this class to show information and progress, or errors to the user.
 * 
 * @author joskuijpers
 */
public interface MergerListener {

	/**
	 * Logs information to the user.
	 * 
	 * @param string text to log.
	 */
	void log(String string);
	
	/**
	 * Handle an exception.
	 * 
	 * Merger execution is halted after this call.
	 * 
	 * @param description description of task that was worked on
	 * @param exception the exception fired (and caught)
	 */
	void exception(String description, Exception exception);
}
