package org.westeroscraft.worldmerger.cli;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.westeroscraft.worldmerger.AppInterface;
import org.westeroscraft.worldmerger.Merger;
import org.westeroscraft.worldmerger.MergerListener;
import org.westeroscraft.worldmerger.config.Configuration;
import org.westeroscraft.worldmerger.config.InvalidConfigurationException;

/**
 * Graphic user interface.
 * 
 * @author joskuijpers
 */
public class CommandLineInterface extends AppInterface {

	/**
	 * Base constructor.
	 * @param args program arguments
	 */
	public CommandLineInterface(String[] args) {
		super(args);
	}

	@Override
	public void run() {
		CommandLine cl;
		Path configPath;
		Configuration conf = null;
		Merger merger;
		
		try {
			cl = parseCommandLine();
		} catch (ParseException e) {
			e.printStackTrace();
			return;
		}
		
		configPath = FileSystems.getDefault().getPath(cl.getOptionValue("config"));
		System.out.println("[LOG] Using configuration at " + configPath);
		
		try {
			conf = Configuration.load(configPath);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
			return;
		}

		merger = new Merger(conf, new CLIHandler());
		merger.run();
	}
	
	private CommandLine parseCommandLine() throws ParseException {
		Options options;
		CommandLineParser parser;
		
		options = new Options();
		parser = new DefaultParser();
		
		options.addOption("nogui", false, "disable gui");
		options.addOption(Option.builder("c").required().argName("file").hasArg().longOpt("config")
				.desc("configuration file").type(Path.class).build());
		
		// TODO: add option to change spawnarea after merging. required better reading of level.dat
		// (no loss)
		
		return parser.parse(options, getProgramArgs(), true);
	}
	
	/**
	 * CLI merger event handler.
	 * 
	 * @author joskuijpers
	 */
	static class CLIHandler implements MergerListener {

		@Override
		public void log(String string) {
			System.out.println("[LOG] " + string);
		}

		@Override
		public void exception(String description, Exception exception) {
			System.out.println("[EXC] Exception during: '" + description + "'");
			exception.printStackTrace();
		}
	}
}
