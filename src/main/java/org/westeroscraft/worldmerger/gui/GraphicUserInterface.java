package org.westeroscraft.worldmerger.gui;

import org.westeroscraft.worldmerger.AppInterface;

/**
 * Command line interface.
 * 
 * @author joskuijpers
 */
public class GraphicUserInterface extends AppInterface {

	/**
	 * Base constructor.
	 * @param args program arguments
	 */
	public GraphicUserInterface(String[] args) {
		super(args);
	}

	@Override
	public void run() {
		// TODO: Implement GUI
	}
}
