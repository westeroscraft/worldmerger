package org.westeroscraft.worldmerger.world.storage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.InflaterInputStream;

import javax.annotation.Nonnull;

import org.westeroscraft.worldmerger.config.XZPair;
import org.westeroscraft.worldmerger.world.NbtCompressionType;

import com.evilco.mc.nbt.stream.NbtInputStream;
import com.evilco.mc.nbt.stream.NbtOutputStream;

/*
 * 8kiB header:
 * 	4kiB locations (1024 entries)
 * 	4kiB timestamps (1024 entries)
 * 
 * location in locationtable using in-region-chunk-coords: 4*((x & 31)+(z & 31) * 32)			
 * x = c % 128, z = c / 128
 *
 * location:
 * 	3 byte offset
 *  1 byte sector count
 *
 * chunk:
 *  4 byte big endian length
 *  1 byte compression type
 *  ... data (length-1 bytes)
 */

/**
 * Loading, changing and saving region files.
 * 
 * @author joskuijpers
 */
public class RegionFile {
	private Path path;
	private RandomAccessFile file;
	private int[] offsets = new int[1024];
	private int[] timestamps = new int[1024];
	private List<Boolean> freeSectors;
	
	private static final byte[] EMPTY_SECTOR = new byte[4096];
	private static final int SECTOR_SIZE = 4096;
	private static final int MAX_CHUNK_SECTORS = 255;

	/**
	 * Load a region file.
	 * @param path path to the region file
	 * @throws IOException reading went wrong
	 */
	public RegionFile(Path path) throws IOException {
		this.path = path;
		this.file = new RandomAccessFile(path.toFile(), "rw");
		
		// If file has no header, it is invalid
		if (file.length() < SECTOR_SIZE) {
			// Add or overwrite bytes for the header
			for (int i = 0; i < 2048; ++i) {
				file.writeInt(0);
			}
		}
		
		// Find free sectors
		freeSectors = new ArrayList<Boolean>();
		
		freeSectors.add(Boolean.FALSE);
		freeSectors.add(Boolean.FALSE);
		for (int i = 2; i < (file.length() / SECTOR_SIZE); ++i) {
			freeSectors.add(Boolean.TRUE);
		}
		
		cacheHeaderAndUpdateFreeSectors();
	}
	
	private void cacheHeaderAndUpdateFreeSectors() throws IOException {
		file.seek(0L);
		
		for (int i = 0; i < 1024; ++i) {
			int offset = file.readInt();
			offsets[i] = offset;
			
			if (offset == 0 || (offset >> 8 + offset & 0xff) > freeSectors.size()) {
				continue;
			}
			
			for (int j = 0; j < (offset & 0xff); ++j) {
				freeSectors.set((offset >> 8) + j, Boolean.FALSE);
			}
		}

		for (int i = 0; i < 1024; ++i) {
			timestamps[i] = file.readInt();
		}
	}
	
	/**
	 * Get whether the given XZ pair is out of bounds.
	 * 
	 * Out of bounds if: x < 0, z < 0, x > 31, z > 31
	 * 
	 * @param chunk the chunk
	 * @return true if out of bounds, false otherwise.
	 */
	private boolean outOfBounds(@Nonnull XZPair chunk) {
		if (chunk == null) {
			return true;
		} else if (chunk.getX() < 0 || chunk.getX() > 31) {
			return true;
		} else if (chunk.getZ() < 0 || chunk.getZ() > 31) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param chunk chunk coords in region
	 * @return true if chunk exists, false otherwise
	 */
	boolean chunkExists(@Nonnull XZPair chunk) {
		int offset;
		
		if (outOfBounds(chunk)) {
			return false;
		}
		
		offset = getChunkOffset(chunk);
		if (offset == 0) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param chunk chunk coords in region
	 * @return -1 if chunk is out of bounds, 0 if chunk not stored, a positive number otherwise.
	 */
	int getChunkTimestamp(@Nonnull XZPair chunk) {
		if (outOfBounds(chunk)) {
			return -1;
		}
		return timestamps[chunk.getZ() * 32 + chunk.getX()];
	}
	
	/**
	 * Get the sector offset of the given chunk in the file.
	 * 
	 * @param chunk chunk coords in region
	 * @return -1 if chunk is out of bounds, 0 if chunk not stored, a positive number otherwise.
	 */
	int getChunkOffset(@Nonnull XZPair chunk) {
		if (outOfBounds(chunk)) {
			return -1;
		}
		return offsets[chunk.getZ() * 32 + chunk.getX()];
	}
	
	/**
	 * Set the timestamp of the chunk.
	 * @param chunk the chunk
	 * @param timestamp the timestamp
	 * @throws IOException when writing goes bad.
	 */
	void setChunkTimestamp(@Nonnull XZPair chunk, int timestamp) throws IOException {
		if (!outOfBounds(chunk)) {
			timestamps[chunk.getZ() * 32 + chunk.getX()] = timestamp;
			file.seek(4096 + (chunk.getZ() * 32 + chunk.getX()) * 4);
			file.writeInt(timestamp);
		}
	}
	
	/**
	 * Set the offset of the chunk.
	 * @param chunk the chunk
	 * @param offset the offset
	 * @throws IOException when writing goes bad.
	 */
	void setChunkOffset(@Nonnull XZPair chunk, int offset) throws IOException {
		if (!outOfBounds(chunk)) {
			offsets[chunk.getZ() * 32 + chunk.getX()] = offset;
			file.seek((chunk.getZ() * 32 + chunk.getX()) * 4);
			file.writeInt(offset);
		}
	}

	/**
	 * Make an uncompressed input stream for the data part of the given chunk.
	 * 
	 * @param chunk the chunk
	 * @return null if out of bounds or chunk does not exist, a stream otherwise
	 * @throws IOException when something goes bad
	 */
	NbtInputStream makeChunkInputStream(@Nonnull XZPair chunk) throws IOException {
		int offset, sector, numSectors, length;
		NbtCompressionType compression;
		byte[] data;
		int read;

		offset = getChunkOffset(chunk); // k
		if (offset == 0 || offset == -1) {
			System.out.println("offset < 1");
			return null;
		}
		
		sector = offset >> 8;
		numSectors = offset & 0xff;
		if (sector + numSectors > freeSectors.size()) {
			return null;
		}
		
		file.seek(sector * SECTOR_SIZE);
		length = file.readInt();
		if (length > SECTOR_SIZE * numSectors || length <= 0) {
			return null;
		}
		
		compression = NbtCompressionType.withValue(file.readByte());
		
		data = new byte[length - 1];
		read = file.read(data);
		
		if (read < length - 1) {
			do {
				read += file.read(data, read, length - 1 - read);
			} while (read != length - 1);
		}
		
		return makeNbtInputStream(new ByteArrayInputStream(data), compression);
	}
	
	/**
	 * Make an uncompressed output stream for the data of the given chunk.
	 * @param chunk the chunk
	 * @return uncompressed nbt output stream
	 * @throws IOException when reading goes bad
	 */
	NbtOutputStream makeChunkOutputStream(@Nonnull XZPair chunk) throws IOException {
		if (outOfBounds(chunk)) {
			return null;
		}
		
		return makeNbtOutputStream(new ChunkOutputBuffer(chunk), NbtCompressionType.ZLIB);
	}

	private NbtInputStream makeNbtInputStream(InputStream stream, NbtCompressionType compression) 
			throws IOException {
		switch (compression) {
		case NONE:
			return new NbtInputStream(stream);
		case GZIP:
			return new NbtInputStream(new GZIPInputStream(stream));
		case ZLIB:
			return new NbtInputStream(new InflaterInputStream(stream));
		default:
			throw new RuntimeException("Unsupported compression type " + compression);
		}
	}
	
	private NbtOutputStream makeNbtOutputStream(OutputStream stream, NbtCompressionType compression)
			throws IOException {
		switch (compression) {
		case NONE:
			return new NbtOutputStream(stream);
		case GZIP:
			return new NbtOutputStream(new GZIPOutputStream(stream));
		case ZLIB:
			return new NbtOutputStream(new DeflaterOutputStream(stream));
		default:
			throw new RuntimeException("Unsupported compression type " + compression);
		}
	}
	
	/**
	 * Write a chunk to the file.
	 * 
	 * @param chunk the chunk
	 * @param data data for the chunk
	 * @param length length of the data
	 * @throws IOException when writing goes bad.
	 */
	@SuppressWarnings("checkstyle:methodlength")
	protected void writeChunk(@Nonnull XZPair chunk, byte[] data, int length) throws IOException {
		int numSectors, sector, offset, currentNumSectors;
		
		numSectors = (length + 5) / SECTOR_SIZE + 1; // +1 for rounding up
		if (numSectors > MAX_CHUNK_SECTORS) {
			throw new RuntimeException("Chunk " + chunk + " is too large. Max size is 1MiB");
		}
		
		offset = getChunkOffset(chunk);

		sector = offset >> 8;
		currentNumSectors = offset & 0xff;
		
		if (currentNumSectors == numSectors && sector != 0) {
			// Already space allocated and same size is needed now
			writeChunk(sector, data, length);
		} else {
			int newOffset;
			
			// Free currently used sectors
			for (int i = 0; i < currentNumSectors; ++i) {
				freeSectors.set(i + sector, Boolean.TRUE);
			}
			
			newOffset = freeSectors.indexOf(Boolean.TRUE);
			int sectionSize = 0;
			
			if (newOffset != -1) {
				// Find the first section with the correct size
				for (int i = newOffset; i < freeSectors.size(); ++i) {
					// If inside a free-block
					if (sectionSize != 0) {
						// if still free, increase section size
						if (freeSectors.get(i).booleanValue()) {
							++sectionSize;
						} else { // if not free, look for new empty section, reset size
							sectionSize = 0;
						}
					} else if (freeSectors.get(i).booleanValue()) {
						// if free, start new empty section
						newOffset = i;
						sectionSize = 1;
					}

					if (sectionSize >= numSectors) {
						break;
					}
				}
			}

			// If section is large enough, use it
			if (sectionSize >= numSectors) {
				sector = newOffset;
				setChunkOffset(chunk, sector << 8 | numSectors);

				for (int i = 0; i < numSectors; ++i) {
					freeSectors.set(sector + i, Boolean.FALSE);
				}

				writeChunk(sector, data, length);
			} else { // add to end of the file
				file.seek(file.length());
				sector = freeSectors.size();

				for (int i = 0; i < numSectors; ++i) {
					file.write(EMPTY_SECTOR);
					freeSectors.add(Boolean.FALSE);
				}

				writeChunk(sector, data, length);
				setChunkOffset(chunk, sector << 8 | numSectors);
			}
		}
		
		setChunkTimestamp(chunk, (int) (System.currentTimeMillis() / 1000L));
	}
	
	/**
	 * Write a chunk.
	 * 
	 * Writes the chunk header and the data.
	 */
	private void writeChunk(int sector, byte[] data, int length) throws IOException {
		file.seek((long) sector * SECTOR_SIZE);
		
		file.writeInt(length);
		file.writeByte(NbtCompressionType.ZLIB.getValue());
		file.write(data, 0, length);
	}
	
	/**
	 * Rewrite the regions location in the world.
	 * 
	 * @param regionCoordinates region-based coordinates
	 * @param runner the runner
	 * @throws IOException when reading or writing goed bad
	 */
	public void runForEachChunk(XZPair regionCoordinates, ChunkRunner runner) throws IOException {
		for (int c = 0; c < 1024; ++c) {
			XZPair chunkInRegionCoords;
			NbtInputStream in;
			NbtOutputStream out;
			Chunk chunk;
			
			chunkInRegionCoords = new XZPair(c % 32, c / 32);
			
			if (!chunkExists(chunkInRegionCoords)) {
				continue;
			}
			
			in = makeChunkInputStream(chunkInRegionCoords);
			chunk = new Chunk(in);
			in.close();

			runner.run(regionCoordinates, chunkInRegionCoords, chunk);

			out = makeChunkOutputStream(chunkInRegionCoords);
			chunk.write(out);
			out.close();
		}
	}

	
	/**
	 * @return the path
	 */
	public Path getPath() {
		return path;
	}

	/**
	 * Interface for running code for every chunk.
	 * 
	 * @author joskuijpers
	 */
	public interface ChunkRunner {
		/**
		 * Run code for each chunk in the region file.
		 * 
		 * @param region coordinates of the region (in region)
		 * @param coordinates coordinates within the region (in chunks)
		 * @param chunk the chunk
		 */
		void run(XZPair region, XZPair coordinates, Chunk chunk);
	}
	
	/**
	 * And output buffer.
	 * 
	 * At closing, it updates the offset table and the size, and manages sectors.
	 * 
	 * @author joskuijpers
	 */
	class ChunkOutputBuffer extends ByteArrayOutputStream {
		private XZPair chunk;
		
		/**
		 * Create a chunk buffer for given chunk.
		 * 
		 * @param chunk the chunk
		 */
		public ChunkOutputBuffer(@Nonnull XZPair chunk) {
			super(SECTOR_SIZE - 5);
			this.chunk = chunk;
			
		}

		@Override
        public void close() throws IOException {
            RegionFile.this.writeChunk(chunk, this.buf, this.count);
        }
	}
}
