package org.westeroscraft.worldmerger.world.storage;

import java.io.IOException;

import javax.annotation.Nonnull;

import org.westeroscraft.worldmerger.config.XZPair;

import com.evilco.mc.nbt.stream.NbtInputStream;
import com.evilco.mc.nbt.stream.NbtOutputStream;
import com.evilco.mc.nbt.tag.TagByte;
import com.evilco.mc.nbt.tag.TagCompound;
import com.evilco.mc.nbt.tag.TagInteger;

/**
 * A chunk in a region file.
 * 
 * @author joskuijpers
 */
public class Chunk {
	/** Root compound of the chunk. */
	private TagCompound rootCompound;
	private TagCompound levelCompound;

	/**
	 * Create a chunk from in-memory data.
	 * 
	 * @param nbt Uncompressed NBT input stream
	 * @throws IOException when reading goes wrong
	 */
	public Chunk(@Nonnull NbtInputStream nbt) throws IOException {
		rootCompound = (TagCompound) nbt.readTag();
		levelCompound = (TagCompound) rootCompound.getTag("Level");
	}
	
	/**
	 * Update the internal chunk coordinates.
	 * @param newCoords new coordinates of the chunk, in world chunk coords
	 */
	public void setLocation(XZPair newCoords) {
		levelCompound.setTag(new TagInteger("xPos", newCoords.getX()));
		levelCompound.setTag(new TagInteger("zPos", newCoords.getZ()));
	}
	
	/**
	 * Get the internal chunk coordinates.
	 * @return chunk coordinates
	 */
	public XZPair getLocation() {
		XZPair coords = new XZPair(0, 0);

		coords.setX(((TagInteger) levelCompound.getTag("xPos")).getValue());
		coords.setZ(((TagInteger) levelCompound.getTag("zPos")).getValue());
		
		return coords;
	}
	
	/**
	 * Save the chunk to data.
	 * 
	 * @param nbt output stream to write to
	 * @throws IOException when writing goes wrong
	 */
	public void write(NbtOutputStream nbt) throws IOException {
		nbt.write(rootCompound);
	}

	/**
	 * @return the levelCompound
	 */
	public TagCompound getLevelCompound() {
		return levelCompound;
	}
	
	/**
	 * Get whether the terrain is populated with trees, ores, special blocks, etc.
	 * 
	 * @return true if populated, false otherwise
	 */
	public boolean getTerrainPopulated() {
		return ((TagByte) levelCompound.getTag("TerrainPopulated")).getValue() == 1;
	}
	
	/**
	 * Set whether the terrain is populated with trees, ores, special blocks, etc.
	 * @param populated whether the terrain is populated
	 */
	public void setTerrainPopulated(boolean populated) {
		byte value = 0;
		if (populated) {
			value = 1;
		}
		
		levelCompound.setTag(new TagByte("TerrainPopulated", value));
	}
	
	/**
	 * @return true if light is populated, false otherwise.
	 */
	public boolean getLightPopulated() {
		return ((TagByte) levelCompound.getTag("LightPopulated")).getValue() == 1;
	}
	
	/**
	 * Set whether the light is populated.
	 * @param populated whether the light is populated
	 */
	public void setLightPopulated(boolean populated) {
		byte value = 0;
		if (populated) {
			value = 1;
		}
		
		levelCompound.setTag(new TagByte("LightPopulated", value));
	}
}
