package org.westeroscraft.worldmerger.world;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.annotation.Nonnull;

import com.evilco.mc.nbt.stream.NbtInputStream;
import com.evilco.mc.nbt.stream.NbtOutputStream;
import com.evilco.mc.nbt.tag.TagByte;
import com.evilco.mc.nbt.tag.TagCompound;
import com.evilco.mc.nbt.tag.TagInteger;
import com.evilco.mc.nbt.tag.TagLong;
import com.evilco.mc.nbt.tag.TagString;

/**
 * Level.dat representation.
 * 
 * @author joskuijpers
 */
public class WorldInfoFile {
	/** The seed of the world. */
	private long randomSeed = 8;
	/** The last time the player was in this world. */
	private long lastTimePlayed = new Date().getTime();
	/** The Game Type. */
    private int gameType;
    /** Whether the map features (e.g. strongholds) generation is enabled or disabled. */
    private boolean mapFeaturesEnabled = false;
	/** Introduced in beta 1.3, is the save version for future control. */
	private int saveVersion = 19133;
	/** The spawn zone position X coordinate. */
	private int spawnX = 0;
	/** The spawn zone position Y coordinate. */
	private int spawnY = 100;
	/** The spawn zone position Z coordinate. */
	private int spawnZ = 0;
	/** The name of the save defined at world creation. */
    private String levelName = "world";
    private boolean allowCommands = true;
	private boolean hardcore = false;
	private byte difficulty = 2;
	private String generatorName = "flat";
	
	/**
	 * Create a new level.dat file.
	 */
	public WorldInfoFile() {
	}
	
	/**
	 * Load level data from file.
	 * 
	 * @param path the path to load from
	 * @throws IOException when reading goes bad
	 */
	public WorldInfoFile(@Nonnull Path path) throws IOException {
		NbtInputStream stream;
		
		stream = new NbtInputStream(new GZIPInputStream(Files.newInputStream(path)));
		
		TagCompound root = (TagCompound) stream.readTag();
		root = (TagCompound) root.getTag("Data");

		lastTimePlayed = ((TagLong) root.getTag("LastPlayed")).getValue();
		randomSeed = ((TagLong) root.getTag("RandomSeed")).getValue();
		gameType = ((TagInteger) root.getTag("GameType")).getValue();
		mapFeaturesEnabled = ((TagByte) root.getTag("MapFeatures")).getValue() == 1;
		saveVersion = ((TagInteger) root.getTag("version")).getValue();
		allowCommands = ((TagByte) root.getTag("allowCommands")).getValue() == 1;
		hardcore = ((TagByte) root.getTag("hardcore")).getValue() == 1;
		levelName = ((TagString) root.getTag("LevelName")).getValue();
		difficulty = ((TagByte) root.getTag("Difficulty")).getValue();
		generatorName = ((TagString) root.getTag("generatorName")).getValue();

		spawnX = ((TagInteger) root.getTag("SpawnX")).getValue();
		spawnY = ((TagInteger) root.getTag("SpawnY")).getValue();
		spawnZ = ((TagInteger) root.getTag("SpawnZ")).getValue();
		
		stream.close();
	}

	/**
	 * Save level data from file.
	 * 
	 * @param path the path to save to
	 * @throws IOException when writing goes bad
	 */
	public void save(@Nonnull Path path) throws IOException {
		NbtOutputStream stream;
		TagCompound data, root;
		
		stream = new NbtOutputStream(new GZIPOutputStream(Files.newOutputStream(path)));
		
		data = new TagCompound("Data");

		data.setTag(new TagLong("LastPlayed", lastTimePlayed));
		data.setTag(new TagLong("RandomSeed", randomSeed));
		data.setTag(new TagInteger("GameType", gameType));
		data.setTag(new TagByte("MapFeatures", booleanToByte(mapFeaturesEnabled)));
		data.setTag(new TagInteger("version", saveVersion));
		data.setTag(new TagByte("allowCommands", booleanToByte(allowCommands)));
		data.setTag(new TagByte("hardcore", booleanToByte(hardcore)));
		data.setTag(new TagString("LevelName", levelName));
		data.setTag(new TagByte("Difficulty", difficulty));
		data.setTag(new TagString("generatorName", generatorName));

		data.setTag(new TagInteger("SpawnX", spawnX));
		data.setTag(new TagInteger("SpawnY", spawnY));
		data.setTag(new TagInteger("SpawnZ", spawnZ));
		
		root = new TagCompound("");
		root.setTag(data);
		
		stream.write(root);		
		stream.close();
	}
	
	private byte booleanToByte(boolean b) {
		if (b) {
			return 1;
		}
		return 0;
	}
}
