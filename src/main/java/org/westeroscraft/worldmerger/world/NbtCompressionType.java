package org.westeroscraft.worldmerger.world;

/**
 * NBT compression types.
 * 
 * @author joskuijpers
 */
public enum NbtCompressionType {
	NONE(0),
	GZIP(1),
	ZLIB(2);
	
	private int value;
	
	private NbtCompressionType(int value) {
		this.value = value;
	}
	
	/**
	 * Get value of the compression.
	 * @return integer value
	 */
	public int getValue() {
		return value;
	}
	
	/**
	 * Get compression type from value.
	 * 
	 * @param value the value
	 * @return the compression type
	 */
	public static NbtCompressionType withValue(int value) {
		switch (value) {
		case 0:
			return NONE;
		case 1:
			return GZIP;
		case 2:
			return ZLIB;
		default:
			throw new IllegalArgumentException("NbtCompressionType " + value + " does not exist");
		}
	}
}
