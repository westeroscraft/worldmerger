package org.westeroscraft.worldmerger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.westeroscraft.worldmerger.cli.CommandLineInterface;
import org.westeroscraft.worldmerger.gui.GraphicUserInterface;

/**
 * Main class.
 * @author joskuijpers
 */
public final class Main {
	
	private Main() {
	}
	
	/**
	 * Application entry point.
	 * @param args program arguments
	 */
	public static void main(String[] args) {
		AppInterface iface;
		
		if (shouldShowGui(args)) {
			iface = new GraphicUserInterface(args);
		} else {
			iface = new CommandLineInterface(args);
		}
		
		iface.run();
	}
	
	private static boolean shouldShowGui(String[] args) {
		Options options;
		CommandLine cmd = null;
		
		options = new Options();
		options.addOption("nogui", false, "disable gui");
				
		try {
			cmd = (new DefaultParser()).parse(options, args, true);
		} catch (ParseException e) {
			e.printStackTrace();
			return true;
		}
		
		return !cmd.hasOption("nogui");
	}
}
