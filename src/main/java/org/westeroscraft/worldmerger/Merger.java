package org.westeroscraft.worldmerger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;

import org.westeroscraft.worldmerger.config.Configuration;
import org.westeroscraft.worldmerger.config.Part;
import org.westeroscraft.worldmerger.config.XZPair;
import org.westeroscraft.worldmerger.world.WorldInfoFile;
import org.westeroscraft.worldmerger.world.storage.Chunk;
import org.westeroscraft.worldmerger.world.storage.RegionFile;

/**
 * Merger runner.
 * 
 * @author joskuijpers
 */
public class Merger {
	private Configuration configuration;
	private MergerListener listener;
	private Path backupDirectory;
	
	/**
	 * Construct a merger with given configuration and a Noop listener.
	 * 
	 * @param configuration the configuration
	 */
	public Merger(Configuration configuration) {
		this.configuration = configuration;
		this.listener = new NoopListener();
	}
	
	/**
	 * Construct a merger with given configuration listener.
	 * 
	 * @param configuration the configuration
	 * @param listener the listener
	 */
	public Merger(@Nonnull Configuration configuration, MergerListener listener) {
		this.configuration = configuration;
		this.listener = listener;
	}
	
	/**
	 * @return the configuration
	 */
	public Configuration getConfiguration() {
		return configuration;
	}

	/**
	 * @return the listener
	 */
	public MergerListener getListener() {
		return listener;
	}

	/**
	 * @param listener the listener to set
	 */
	public void setListener(MergerListener listener) {
		if (listener == null) {
			listener = new NoopListener();
		}
		
		this.listener = listener;
	}

	// todo: some progress thing / handler delegate for messages and stuff.
	// handler can be for CLI just sysout, for GUI some log box. handler.log, .error, 
	// .status if you will!
	
	/**
	 * Run the merger using the set configuration.
	 */
	public void run() {
		try {
			listener.log("verifying and/or creating output folder structure");
			
			manageOutfolder();
		} catch (IOException ioe) {
			listener.exception("managing out folder", ioe);
			return;
		}
		
		try {
			listener.log("verifying and/or creating level.dat");
			
			manageLevelDat();
		} catch (IOException ioe) {
			listener.exception("managing level.dat", ioe);
			return;
		}
		
		acquireLock();
		
		try {
			listener.log("copying regions");
			
			copyFiles();
		} catch (IOException ioe) {
			listener.exception("copying regions", ioe);
			return;			
		}
		
		try {
			listener.log("rewriting internal chunk locations");
			
			rewriteLocations();
		} catch (IOException ioe) {
			listener.exception("rewriting internal chunk locations", ioe);
			return;			
		}
		
		try {
			listener.log("renaming temporary files");
			
			renameFiles();
		} catch (IOException ioe) {
			listener.exception("renaming temporary files", ioe);
			return;
		}
		
		releaseLock();
		
		listener.log("finished");
	}
	
	private String filenameForRegion(XZPair region) {
		return "r." + region.getX() + "." + region.getZ() + ".mca";
	}
	
	/**
	 * Manage the output folder, creating and validating folders and files.
	 * @throws IOException
	 */
	private void manageOutfolder() throws IOException {
		Path root = configuration.getOutput().getPath();
		
		// Create out folder
		if (Files.notExists(root)) {
			Files.createDirectories(root.resolve("region"));
			listener.log("created world folder");
		}
		
		// Create region folder
		if (Files.notExists(root.resolve("region"))) {
			Files.createDirectories(root.resolve("region"));
			listener.log("created region folder");
		}

		// Create backup folder
		if (Files.notExists(root.resolve("bak"))) {
			backupDirectory = root.resolve("bak");
			Files.createDirectories(backupDirectory);
			
			listener.log("created backup directory " + backupDirectory);
		} else {
			String[] list = null;

			list = root.resolve("bak").toFile().list();

			if (list != null && list.length > 0) {
				int c = 1;

				listener.log("default backup directory already exists and is non empty");

				do {
					++c;
					backupDirectory = root.resolve("bak" + c);

					if (Files.exists(backupDirectory)) {
						list = backupDirectory.toFile().list();
					} else {
						break;
					}
				} while (list != null && list.length > 0);

				Files.createDirectories(backupDirectory);

				listener.log("using (created) backup directory " + backupDirectory);
			} else {
				backupDirectory = root.resolve("bak");

				listener.log("using existing empty backup folder " + backupDirectory);
			}

		}
	}

	private void manageLevelDat() throws IOException {
		Path root = configuration.getOutput().getPath();
		
		// does level.dat exist?
		if (Files.notExists(root.resolve("level.dat"))) {
			listener.log("creating level.dat file...");
			
			new WorldInfoFile().save(root.resolve("level.dat"));
		}
	}

	private void copyFiles() throws IOException {
		Path outRegionFolder = configuration.getOutput().getPath().resolve("region");

		// Parts handling
		for (Part part : configuration.getParts()) {
			XZPair origin = part.getOrigin().toRegionPair();
			XZPair size = part.getSize().toRegionPair();
			XZPair out = part.getOut().toRegionPair();
			Path inFolder = part.getPath().resolve("region");

			listener.log("handling region from world " + part.getPath().getFileName());

			// For every region in the area
			for (int x = 0; x < size.getX(); ++x) {
				for (int z = 0; z < size.getZ(); ++z) {
					XZPair originalLocation = new XZPair(origin.getX() + x, origin.getZ() + z);
					XZPair newLocation = new XZPair(out.getX() + x, out.getZ() + z);

					listener.log("  copying region " + originalLocation + " to " + newLocation);

					Path originalPath = inFolder.resolve(filenameForRegion(originalLocation));
					Path newPath = outRegionFolder.resolve("~" + filenameForRegion(newLocation));

					// System.out.println("copy from "+originalPath+" to "+newPath);
					// File should exist
					if (Files.notExists(originalPath)) {
						throw new RuntimeException("Region file at " + originalPath
								+ " expected for part in world " + part.getPath().getFileName()
								+ ".");
					}

					// Copy the file
					Files.copy(originalPath, newPath, StandardCopyOption.REPLACE_EXISTING);
				}
			}
		}
	}
	
	private void rewriteLocations() throws IOException {
		Path outRegionFolder = configuration.getOutput().getPath().resolve("region");
		Pattern filePattern;
		String[] list;

		filePattern = Pattern.compile("~r.(-?\\d+).(-?\\d+).mca");
		list = outRegionFolder.toFile().list();
		
		if (list == null) {
			throw new RuntimeException("Failed to read region list for rewriting locations");
		}

		for (String name : list) {
			RegionFile region;
			Path path;
			XZPair regionCoords;
			Matcher matcher;

			if (!name.startsWith("~")) {
				continue;
			}

			path = outRegionFolder.resolve(name);

			// Load region
			region = new RegionFile(path);

			// Get new region position from filename
			matcher = filePattern.matcher(name);
			if (!matcher.matches()) {
				throw new RuntimeException("new region file name can't be parsed: " + name);
			}

			regionCoords = new XZPair(
					Integer.parseInt(matcher.group(1)),
					Integer.parseInt(matcher.group(2)));

			listener.log("rewriting chunk locations for region at " + regionCoords);

			// Update coordinates
			region.runForEachChunk(regionCoords, new RegionFile.ChunkRunner() {
				@Override
				public void run(XZPair region, XZPair coordinates, Chunk chunk) {
					XZPair newCoords;
					
					newCoords = new XZPair(
							region.getX() * 32 + coordinates.getX(),
							region.getZ() * 32 + coordinates.getZ());
					chunk.setLocation(newCoords);

					chunk.setLightPopulated(false);
				}
			});
		}
	}
	
	private void renameFiles() throws IOException {
		Path outRegionFolder = configuration.getOutput().getPath().resolve("region");
		String[] list = outRegionFolder.toFile().list();
		
		if (list == null) {
			throw new RuntimeException("Failed to read region directory for renaming");
		}
		
		for (String name : list) {
			if (!name.startsWith("~")) {
				continue;
			}

			// Path to the file with the tilde
			Path tmpFile = outRegionFolder.resolve(name);

			// Path to the file we need to end up with
			String noTmpName = name.substring(1, name.length());
			Path file = outRegionFolder.resolve(noTmpName);

			// If that file already exists, back it up
			if (Files.exists(file)) {
				listener.log("backing up " + file);
				Files.move(file, backupDirectory.resolve(noTmpName),
						StandardCopyOption.REPLACE_EXISTING);
			}

			// Remove tilde
			listener.log("renaming " + tmpFile);
			Files.move(tmpFile, file);
		}
	}
	
	/**
	 * Acquire the session lock.
	 */
	private void acquireLock() {
		Path lockPath = configuration.getOutput().getPath().resolve("session.lock");

		// Write current timestamp in 64bit milliseconds big-endian
		Date now = new Date();
		long ms = now.getTime();
		
		try (FileOutputStream os = new FileOutputStream(lockPath.toFile())) {
			FileChannel fc = os.getChannel();
			
			ByteBuffer bb = ByteBuffer.allocate(8);
			bb.order(ByteOrder.BIG_ENDIAN);
			bb.putLong(ms);
			bb.flip();

			fc.write(bb);
		} catch (IOException e) {
			listener.exception("writing to session.lock", e);
			return;
		}
		
		// TODO Monitor for changes in session.lock
		
		// If changed, abort
	}
	
	/**
	 * Release the session lock.
	 */
	private void releaseLock() {
		
	}
	
	/**
	 * A listener that does absolutely nothing.
	 * 
	 * @author joskuijpers
	 */
	static class NoopListener implements MergerListener {

		/**
		 * No-args constructor.
		 */
		public NoopListener() {
		}

		@Override
		public void log(String string) {
		}

		@Override
		public void exception(String description, Exception exception) {
			exception.printStackTrace();
		}
		
	}
}
